import math
import random
import sys


def calculating_pi(radiusX=200, iterations=1000):
    radius = radiusX * 100
    dots_inside_circle = 0
    for _ in range(iterations):
        # Getting a random dot inside a square around the circle
        x = random.randint(-radius, radius)
        y = random.randint(-radius, radius)

        # Apply Pythagoras Formula to find out if the point position is in or out the circle
        if (math.pow(x, 2) + math.pow(y, 2)) < math.pow(radius, 2):
            dots_inside_circle += 1

    # Applying Monte Carlo's Method to estimate Pi
    pi = 4 * (dots_inside_circle / float(iterations))
    print('PI approximation using', iterations, 'points:', pi)


def dice_casino_game(bet=1, iterations=1000):
    # Roll Dice Function
    def roll_dice():
        d1 = random.randint(1, 6)
        d2 = random.randint(1, 6)

        # Determining if the dice are the same number
        if d1 == d2:
            return True
        return False

    win_probability = []
    end_balance = []
    for i in range(iterations):
        balance = [1000]
        num_rolls = [0]
        num_wins = 0
        # Run until the player has rolled 1,000 times
        while num_rolls[-1] < 1000:
            same = roll_dice()
            # Result if the dice are the same number
            if same:
                balance.append(balance[-1] + 4 * bet)
                num_wins += 1
            # Result if the dice are different numbers
            else:
                balance.append(balance[-1] - bet)

            num_rolls.append(num_rolls[-1] + 1)
        # Store tracking variables and add line to figure
        win_probability.append(num_wins / num_rolls[-1])
        end_balance.append(balance[-1])

    # Averaging win probability and end balance
    overall_win_probability = sum(win_probability)/len(win_probability)
    overall_end_balance = sum(end_balance)/len(end_balance)
    print(f'Average win probability after {iterations} runs: {overall_win_probability}')
    print(f'Average ending balance after {iterations} runs: ${overall_end_balance}')


# Calculates the probability of finding orders of faulty chips produced in a factory.
def electronic_chips_production(max_bad_items=5, production_items=18, iterations=1000):
    # Counts the faulty orders in production
    def faulty_orders(production):
        # Organizes the given container as pairs.
        def pairs(production):
            for i in range(1, len(production)):
                yield production[i - 1], production[i]
            yield production[-1], production[0]

        i = 0
        for x1, x2 in pairs(production):
            # Faulty order when the first item is True and the second False
            if x1 and not x2:
                i += 1
        return i

    bad_productions = 0
    for _ in range(iterations):
        production = [random.choice([True, False]) for _ in range(production_items)]
        # Check if the production is faulty
        if max_bad_items < faulty_orders(production):
            bad_productions += 1

    print(
        'probability of getting', max_bad_items,
        'faulty productions in', iterations,
        'productions:', f'{(bad_productions / float(iterations)) * 100} %',
    )

if __name__ == '__main__':
    start_program = True
    while start_program:
        print('')
        print('Please select a MC simulation:')
        print('1. Calculating Pi')
        print('2. Casino roll dices')
        print('3. Chips factory production')
        print('')
        print('9. Quit')
        print('')
        user_selection = input('Enter simulation number: ')
        if int(user_selection) == 1:
            radius = input('Enter radius length: ')
            iterations = input('Enter number of iterations: ')
            print('')
            calculating_pi(int(radius), int(iterations))
        elif int(user_selection) == 2:
            bet = input('Enter bet amount: ')
            iterations = input('Enter number of iterations: ')
            print('')
            dice_casino_game(int(bet), int(iterations))
        elif int(user_selection) == 3:
            production_items = input('Enter chips count per production: ')
            max_bad_items = input('Enter max bad chips count per production: ')
            iterations = input('Enter number of iterations: ')
            print('')
            electronic_chips_production(int(max_bad_items), int(production_items), int(iterations))
        elif int(user_selection) == 9:
            sys.exit()
        else:
            print('Invalid selection')
